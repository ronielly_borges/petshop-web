﻿namespace PetShopWeb.Enums
{
    public enum StatusAccommodation
    {
        Free = 0,
        Busy = 1,
        WaitingOwner = 2
    }
}
