﻿using System.ComponentModel.DataAnnotations;

namespace PetShopWeb.Enums
{
    public enum StatusCurrentHealthEnum
    {
        [Display(Name = "In Treament")]
        InTreament = 1,
        Recovering = 2,
        Recovered = 3
    }
}
