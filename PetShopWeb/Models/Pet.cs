﻿using Microsoft.AspNetCore.Mvc.Rendering;
using PetShopWeb.Enums;
using System.Collections.Generic;
using System.ComponentModel;

namespace PetShopWeb.Models
{
    public class Pet
    {
        public Pet Update(Pet pet)
        {
            Name = pet.Name;
            ReasonHospitalization = pet.ReasonHospitalization;
            StatusCurrentHealth = pet.StatusCurrentHealth;
            Photograph = pet.Photograph;

            return this;
        }

        public int Id { get; set; }
        public string Name { get; set; }

        [DisplayName("Reason Hospitalization")]
        public string ReasonHospitalization { get; set; }

        [DisplayName("Status Current Health")]
        public StatusCurrentHealthEnum StatusCurrentHealth { get; set; }
        public string Photograph { get; set; }

        [DisplayName("Owner")]
        public int OwnerId { get; set; }
        public virtual Owner Owner { get; set; }

        [DisplayName("Free accommodations")]
        public int AccommodationId { get; set; }
        public virtual Accommodation Accommodation { get; set; }
    }
}
