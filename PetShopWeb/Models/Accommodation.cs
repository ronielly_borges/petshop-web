﻿using PetShopWeb.Enums;
using System.ComponentModel;

namespace PetShopWeb.Models
{
    public class Accommodation
    {
        public int Id { get; set; }
        [DisplayName("Accomodation")]
        public int Number { get; set; }
        [DisplayName("Status")]
        public StatusAccommodation StatusAccommodationEnum { get; set; }
    }
}
