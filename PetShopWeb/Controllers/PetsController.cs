﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PetShopWeb.Data;
using PetShopWeb.Models;

namespace PetShopWeb.Controllers
{
    public class PetsController : Controller
    {
        private readonly Context _context;

        public PetsController(Context context)
        {
            _context = context;
        }

        // GET: Pets
        public async Task<IActionResult> Index()
        {
            var context = _context.Pets.Include(p => p.Accommodation).Include(p => p.Owner);
            return View(await context.ToListAsync());
        }

        // GET: Pets/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pet = await _context.Pets
                .Include(p => p.Accommodation)
                .Include(p => p.Owner)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (pet == null)
            {
                return NotFound();
            }

            return View(pet);
        }

        // GET: Pets/Create
        public IActionResult Create()
        {
            var accommodationsFree = _context.Accommodations.Where(accommodation => accommodation.StatusAccommodationEnum == Enums.StatusAccommodation.Free);
            ViewData["AccommodationId"] = new SelectList(accommodationsFree, "Id", "Number"); ;

            ViewData["OwnerId"] = new SelectList(_context.Owners, "Id", "Name");

            return View();
        }

        // POST: Pets/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,ReasonHospitalization,StatusCurrentHealth,Photograph,OwnerId,AccommodationId")] Pet pet)
        {
            if (ModelState.IsValid)
            {
                _context.Add(pet);

                var accommodation = await _context.Accommodations.FindAsync(pet.AccommodationId);

                if (accommodation == null)
                    return NotFound();

                accommodation.StatusAccommodationEnum = Enums.StatusAccommodation.Busy;
                _context.Update(accommodation);

                await _context.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }
            
            ViewData["AccommodationId"] = new SelectList(_context.Accommodations, "Id", "Id", pet.AccommodationId);
            ViewData["OwnerId"] = new SelectList(_context.Owners, "Id", "Id", pet.OwnerId);
            return View(pet);
        }

        // GET: Pets/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pet = await _context.Pets.FindAsync(id);
            if (pet == null)
            {
                return NotFound();
            }
            ViewData["AccommodationId"] = new SelectList(_context.Accommodations, "Id", "Id", pet.AccommodationId);
            ViewData["OwnerId"] = new SelectList(_context.Owners, "Id", "Id", pet.OwnerId);
            return View(pet);
        }

        // POST: Pets/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,ReasonHospitalization,StatusCurrentHealth,Photograph,OwnerId,AccommodationId")] Pet pet)
        {
            if (id != pet.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(pet);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PetExists(pet.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AccommodationId"] = new SelectList(_context.Accommodations, "Id", "Id", pet.AccommodationId);
            ViewData["OwnerId"] = new SelectList(_context.Owners, "Id", "Id", pet.OwnerId);
            return View(pet);
        }

        // GET: Pets/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pet = await _context.Pets
                .Include(p => p.Accommodation)
                .Include(p => p.Owner)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (pet == null)
            {
                return NotFound();
            }

            return View(pet);
        }

        // POST: Pets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var pet = await _context.Pets.FindAsync(id);
            _context.Pets.Remove(pet);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PetExists(int id)
        {
            return _context.Pets.Any(e => e.Id == id);
        }
    }
}
