﻿using Microsoft.EntityFrameworkCore;
using PetShopWeb.Models;

namespace PetShopWeb.Data
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> options) : base(options)
        {
        }

        public DbSet<Pet> Pets { get; set; }
        public DbSet<Owner> Owners { get; set; }
        public DbSet<Accommodation> Accommodations { get; set; }
    }
}
