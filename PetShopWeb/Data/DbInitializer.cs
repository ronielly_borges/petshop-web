﻿using PetShopWeb.Models;
using System.Linq;

namespace PetShopWeb.Data
{
    public static class DbInitializer
    {
        public static void Initialize(Context context) 
        {
            context.Database.EnsureCreated();

            if (context.Accommodations.Any())
            {
                return;
            }

            var accommodations = new Accommodation[]
            {
                new Accommodation { Number = 1, StatusAccommodationEnum = Enums.StatusAccommodation.Free },
                new Accommodation { Number = 2, StatusAccommodationEnum = Enums.StatusAccommodation.Free },
                new Accommodation { Number = 3, StatusAccommodationEnum = Enums.StatusAccommodation.Free },
                new Accommodation { Number = 4, StatusAccommodationEnum = Enums.StatusAccommodation.Free },
                new Accommodation { Number = 5, StatusAccommodationEnum = Enums.StatusAccommodation.Free },
                new Accommodation { Number = 6, StatusAccommodationEnum = Enums.StatusAccommodation.Free },
                new Accommodation { Number = 7, StatusAccommodationEnum = Enums.StatusAccommodation.Free },
                new Accommodation { Number = 8, StatusAccommodationEnum = Enums.StatusAccommodation.Free },
                new Accommodation { Number = 9, StatusAccommodationEnum = Enums.StatusAccommodation.Free },
                new Accommodation { Number = 10, StatusAccommodationEnum = Enums.StatusAccommodation.Free }
            };

            foreach(Accommodation acc in accommodations)
            {
                context.Accommodations.Add(acc);
            }

            context.SaveChanges();
        }
    }
}
